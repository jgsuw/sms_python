import matplotlib.pyplot as plt
import pickle
import numpy as np
# import pygame
import sys
import os
import time
import math
from random import shuffle
import protocol

args = sys.argv
testRun = True
numIters = 600
trialType = "-Adaptive"

# TODO: have to figure out how many trials are necessary to test, with taking into account parametrization

def setup(args):
    args = sys.argv
    if len(args) < 2:
        print("No subject specified.")
        sys.exit()
    subject = args[1]
    exp_data_dir = "exp_data"
    subject_dir = os.path.join(exp_data_dir,subject)
    if not os.path.exists(subject_dir):
        os.mkdir(subject_dir)

    return subject, subject_dir

def datestring(t=None,sec=False):
  """
  Datestring

  Inputs:
    (optional)
    t - time.localtime()
    sec - bool - whether to include sec [SS] in output

  Outputs:
    ds - str - date in YYYYMMDD-HHMM[SS] format

  by Sam Burden 2012
  """
  if t is None:
    import time
    t = time.localtime()

  ye = '%04d'%t.tm_year
  mo = '%02d'%t.tm_mon
  da = '%02d'%t.tm_mday
  ho = '%02d'%t.tm_hour
  mi = '%02d'%t.tm_min
  se = '%02d'%t.tm_sec
  if not sec:
    se = ''

  return ye+mo+da+'-'+ho+mi+se

def pickle_data(subject, subject_dir, trialData):
    filename = datestring()
    filename += subject
    filename += trialType
    filename += ".pickle"

    pkl_file = os.path.join(subject_dir, filename)
    save_file = open(pkl_file, "wb")
    pickle.dump(trialData, save_file)
    save_file.close()

def get_experimental_data():

    args = []
    trialData = []

    # a test trial to have the user just get a trial where they can
    # attempt to synchronize -- no adaptation, no data collection
    if testRun:
        trialName = "testRun"
        trialTm = .48e3
        # trialTm = .5e3

        isAdaptive = True
        alpha_m = .5
        beta_m = .0
        adaptiveParams = [isAdaptive, alpha_m, beta_m]
        args = (trialName, trialTm, adaptiveParams)
        protocol.main(*args)

    time.sleep(5)
    experimentParams = {
        "slow1": [.9e3, True, .2, 0],
        "slow2": [.9e3, True, .4, 0],
        "slow3": [.9e3, True, .6, 0],
        "slow4": [.9e3, True, .8, 0],
        "slow5": [.9e3, True, 1., 0],
        "mid1": [.65e3, True, .2, 0],
        "mid2": [.65e3, True, .4, 0],
        "mid3": [.65e3, True, .6, 0],
        "mid4": [.65e3, True, .8, 0],
        "mid5": [.65e3, True, 1., 0],
        "fast1": [.5e3, True, .2, 0],
        "fast2": [.5e3, True, .4, 0],
        "fast3": [.5e3, True, .6, 0],
        "fast4": [.5e3, True, .8, 0],
        "fast5": [.5e3, True, 1., 0],
    }

    experimentOrder = list(experimentParams.keys())
    shuffle(experimentOrder)

    print(experimentOrder)
    for i in range(len(experimentOrder)):
        trialName = experimentOrder[i]
        print(trialName)
        print(i)

        trialTm = experimentParams[trialName][0]
        isAdaptive = experimentParams[trialName][1]
        alpha_m = experimentParams[trialName][2]
        beta_m = experimentParams[trialName][3]

        adaptiveParams = [isAdaptive, alpha_m, beta_m]
        args = (trialName, trialTm, adaptiveParams)
        trialData += [protocol.main(*args)]

        time.sleep(5)

    return trialData

subject, subject_dir = setup(args)
trialData = get_experimental_data()
pickle_data(subject, subject_dir, trialData)

