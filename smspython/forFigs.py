import matplotlib.pyplot as plt
import numpy as np
import pickle

def grab_data(filepath):
    infile = open(filepath, 'rb')
    trialData = pickle.load(infile)
    infile.close()

    return trialData

def grab_alpha_beta_points():
    n = 3
    for i in range(n):
        andrewTrial = andrewData[i]
        temp = []
        for j in range(len(andrewTrial)):
            alpha = andrewTrial[j][4][0]
            beta = andrewTrial[j][4][1]
            temp.append([alpha, beta])
        if i == 0:
            slowDataPoints.append(temp)
        elif i == 1:
            medDataPoints.append(temp)
        elif i == 2:
            fastDataPoints.append(temp)

        autumnTrial = autumnData[i]
        temp = []
        for j in range(len(autumnTrial)):
            alpha = autumnTrial[j][4][0]
            beta = autumnTrial[j][4][1]
            temp.append([alpha, beta])
        if i == 0:
            slowDataPoints.append(temp)
        elif i == 1:
            medDataPoints.append(temp)
        elif i == 2:
            fastDataPoints.append(temp)

        guyTrial = guyData[i]
        temp = []
        for j in range(len(guyTrial)):
            alpha = guyTrial[j][4][0]
            beta = guyTrial[j][4][1]
            temp.append([alpha, beta])
        if i == 0:
            slowDataPoints.append(temp)
        elif i == 1:
            medDataPoints.append(temp)
        elif i == 2:
            fastDataPoints.append(temp)

        rayTrial = rayData[i]
        temp = []
        for j in range(len(rayTrial)):
            alpha = rayTrial[j][4][0]
            beta = rayTrial[j][4][1]
            temp.append([alpha, beta])
        if i == 0:
            slowDataPoints.append(temp)
        elif i == 1:
            medDataPoints.append(temp)
        elif i == 2:
            fastDataPoints.append(temp)

        rickyTrial = rickyData[i]
        temp = []
        for j in range(len(rickyTrial)):
            alpha = rickyTrial[j][4][0]
            beta = rickyTrial[j][4][1]
            temp.append([alpha, beta])
        if i == 0:
            slowDataPoints.append(temp)
        elif i == 1:
            medDataPoints.append(temp)
        elif i == 2:
            fastDataPoints.append(temp)

# get everyone's data
filepathAP = "smspython/data_for_figs/20190813-175437Andrew_Pace-trialInfo.pickle"
filepathAH = "smspython/data_for_figs/20190813-175539Autumn_Hughes-trialInfo.pickle"
filepathGH = "smspython/data_for_figs/20190813-175701Guyton_Harvey-trialInfo.pickle"
filepathRS = "smspython/data_for_figs/20190813-175828Raymond_Surya-trialInfo.pickle"
filepathRW = "smspython/data_for_figs/20190813-175854Ricky_Wright-trialInfo.pickle"

andrewData = grab_data(filepathAP)
autumnData = grab_data(filepathAH)
guyData = grab_data(filepathGH)
rayData = grab_data(filepathRS)
rickyData = grab_data(filepathRW)

slowDataPoints = []
medDataPoints = []
fastDataPoints = []
grab_alpha_beta_points()

# print(len(slowDataPoints))
# print(len(medDataPoints))
# print(len(fastDataPoints))
# for i in range(len(slowDataPoints)):
#     print(slowDataPoints[i])
# print()
# for i in range(len(medDataPoints)):
#     print(medDataPoints[i])
# print()
# for i in range(len(fastDataPoints)):
#     print(fastDataPoints[i])

from matplotlib.patches import Ellipse
import matplotlib.transforms as transforms

def confidence_ellipse(x, y, ax, n_std=3.0, facecolor='none', **kwargs):
    """
    Create a plot of the covariance confidence ellipse of *x* and *y*.

    Parameters
    ----------
    x, y : array-like, shape (n, )
        Input data.

    ax : matplotlib.axes.Axes
        The axes object to draw the ellipse into.

    n_std : float
        The number of standard deviations to determine the ellipse's radiuses.

    Returns
    -------
    matplotlib.patches.Ellipse

    Other parameters
    ----------------
    kwargs : `~matplotlib.patches.Patch` properties
    """
    if x.size != y.size:
        raise ValueError("x and y must be the same size")

    cov = np.cov(x, y)
    pearson = cov[0, 1]/np.sqrt(cov[0, 0] * cov[1, 1])
    # Using a special case to obtain the eigenvalues of this
    # two-dimensionl dataset.
    ell_radius_x = np.sqrt(1 + pearson)
    ell_radius_y = np.sqrt(1 - pearson)
    ellipse = Ellipse((0, 0),
        width=ell_radius_x * 2,
        height=ell_radius_y * 2,
        facecolor=facecolor,
        **kwargs)

    # Calculating the stdandard deviation of x from
    # the squareroot of the variance and multiplying
    # with the given number of standard deviations.
    scale_x = np.sqrt(cov[0, 0]) * n_std
    mean_x = np.mean(x)

    # calculating the stdandard deviation of y ...
    scale_y = np.sqrt(cov[1, 1]) * n_std
    mean_y = np.mean(y)

    transf = transforms.Affine2D() \
        .rotate_deg(45) \
        .scale(scale_x, scale_y) \
        .translate(mean_x, mean_y)

    ellipse.set_transform(transf + ax.transData)
    return ax.add_patch(ellipse)

def add_user_ellipses(ax, datapoints, colors=['r', 'g', 'b', 'c', 'm']):
    sdp = np.array(datapoints)
    
    for i,c in enumerate(colors):
        alphas = sdp[i, :,0]
        betas = sdp[i,:,1]
        ax.plot(np.mean(alphas), np.mean(betas), c+'o')
        confidence_ellipse(alphas, betas, ax, n_std=1, edgecolor=c, facecolor=c, alpha=.3)

def graph_avb(slowDataPoints, medDataPoints, fastDataPoints):

    fig = plt.figure()
    ax = plt.subplot(131)
    for i in range(len(slowDataPoints)):
        userPoints = slowDataPoints[i]
        for j in range(len(userPoints)):
            alpha = userPoints[j][0]
            beta = userPoints[j][1]
            if i == 0:
                if j == 0:
                    ax.plot(alpha, beta, 'rP', label="Subject 1")
                else:
                    ax.plot(alpha, beta, 'rP')
            elif i == 1:
                if j == 0:
                    ax.plot(alpha, beta, 'gP', label="Subject 2")
                else:
                    ax.plot(alpha, beta, 'gP')
            elif i == 2:
                if j == 0:
                    ax.plot(alpha, beta, 'bP', label="Subject 3")
                else:
                    ax.plot(alpha, beta, 'bP')
            elif i == 3:
                if j == 0:
                    ax.plot(alpha, beta, 'cP', label="Subject 4")
                else:
                    ax.plot(alpha, beta, 'cP')
            elif i == 4:
                if j == 0:
                    ax.plot(alpha, beta, 'mP', label="Subject 5")
                else:
                    ax.plot(alpha, beta, 'mP')

    add_user_ellipses(ax, slowDataPoints)

    # ax.set_title("Slow IOI (900 ms)")
    plt.setp(ax.get_yticklabels(), fontsize=15)
    plt.setp(ax.get_xticklabels(), fontsize=15)
    # plt.legend()

    ax = plt.subplot(132)
    for i in range(len(medDataPoints)):
        userPoints = medDataPoints[i]
        for j in range(len(userPoints)):
            alpha = userPoints[j][0]
            beta = userPoints[j][1]
            if i == 0:
                if j == 0:
                    ax.plot(alpha, beta, 'rP', label="Subject 1")
                else:
                    ax.plot(alpha, beta, 'rP')
            elif i == 1:
                if j == 0:
                    ax.plot(alpha, beta, 'gP', label="Subject 2")
                else:
                    ax.plot(alpha, beta, 'gP')
            elif i == 2:
                if j == 0:
                    ax.plot(alpha, beta, 'bP', label="Subject 3")
                else:
                    ax.plot(alpha, beta, 'bP')
            elif i == 3:
                if j == 0:
                    ax.plot(alpha, beta, 'cP', label="Subject 4")
                else:
                    ax.plot(alpha, beta, 'cP')
            elif i == 4:
                if j == 0:
                    ax.plot(alpha, beta, 'mP', label="Subject 5")
                else:
                    ax.plot(alpha, beta, 'mP')
    
    add_user_ellipses(ax, medDataPoints)

    plt.setp(ax.get_yticklabels(), fontsize=15)
    plt.setp(ax.get_xticklabels(), fontsize=15)
    # ax.set_title("Medium IOI (650 ms)")
    plt.legend(prop={'size': 15})

    ax = plt.subplot(133)
    for i in range(len(fastDataPoints)):
        userPoints = fastDataPoints[i]
        for j in range(len(userPoints)):
            alpha = userPoints[j][0]
            beta = userPoints[j][1]
            if i == 0:
                if j == 0:
                    ax.plot(alpha, beta, 'rP', label="Subject 1")
                else:
                    ax.plot(alpha, beta, 'rP')
            elif i == 1:
                if j == 0:
                    ax.plot(alpha, beta, 'gP', label="Subject 2")
                else:
                    ax.plot(alpha, beta, 'gP')
            elif i == 2:
                if j == 0:
                    ax.plot(alpha, beta, 'bP', label="Subject 3")
                else:
                    ax.plot(alpha, beta, 'bP')
            elif i == 3:
                if j == 0:
                    ax.plot(alpha, beta, 'cP', label="Subject 4")
                else:
                    ax.plot(alpha, beta, 'cP')
            elif i == 4:
                if j == 0:
                    ax.plot(alpha, beta, 'mP', label="Subject 5")
                else:
                    ax.plot(alpha, beta, 'mP')
    plt.setp(ax.get_yticklabels(), fontsize=15)
    plt.setp(ax.get_xticklabels(), fontsize=15)
    # ax.set_title("Fast IOI (500 ms)")

    add_user_ellipses(ax, fastDataPoints)

    handles, labels = ax.get_legend_handles_labels()
    # fig.legend(handles, labels, loc='upper right', prop={'size': 12})
    # plt.tight_layout()
    plt.show()

graph_avb(slowDataPoints, medDataPoints, fastDataPoints)

def graph_params(guyData, rickyData):

    guySlowActual = guyData[0][1][0]
    guySlowSims = guyData[0][1][1]
    guyMedActual = guyData[1][0][0]
    guyMedSims = guyData[1][0][1]
    guyFastActual = guyData[2][4][0]
    guyFastSims = guyData[2][4][1]

    print(len(guySlowActual))
    print(len(guySlowSims))
    print(guySlowActual)
    print(guySlowSims)

    rickySlowActual = rickyData[0][1][0]
    rickySlowSims = rickyData[0][1][1]
    rickyMedActual = rickyData[1][3][0]
    rickyMedSims = rickyData[1][3][1]
    rickyFastActual = rickyData[2][4][0]
    rickyFastSims = rickyData[2][4][1]

    fig = plt.figure()
    ax = plt.subplot(231)
    plt.scatter(range(len(guySlowSims)), guySlowSims, marker="D", label="Simulated")
    plt.scatter(range(len(guySlowActual)), guySlowActual, c='r', label="Experimental")
    zeroLine = np.zeros(len(guySlowSims))
    trialIters = np.linspace(0, len(guySlowSims), len(guySlowSims))
    ax.plot(trialIters, zeroLine, 'k')
    # ax.set_title("Slow IOI (900 ms)", fontsize='25')
    plt.xticks(np.arange(0, len(guySlowSims), 5))
    plt.setp(ax.get_yticklabels(), fontsize=15)
    plt.setp(ax.get_xticklabels(), fontsize=15)
    # plt.legend()

    ax = plt.subplot(232)
    plt.scatter(range(len(guyMedSims)), guyMedSims, marker="D", label="Simulated")
    plt.scatter(range(len(guyMedActual)), guyMedActual, c='r', label="Experimental")
    zeroLine = np.zeros(len(guyMedActual))
    trialIters = np.linspace(0, len(guyMedSims), len(guyMedSims))
    ax.plot(trialIters, zeroLine, 'k')
    # ax.set_title("Medium IOI (650 ms)", fontsize='25')
    plt.xticks(np.arange(0, len(guyMedSims), 5))
    plt.setp(ax.get_yticklabels(), fontsize=15)
    plt.setp(ax.get_xticklabels(), fontsize=15)
    plt.legend(prop={'size': 17})

    ax = plt.subplot(233)
    plt.scatter(range(len(guyFastSims)), guyFastSims, marker="D", label="Simulated")
    plt.scatter(range(len(guyFastActual)), guyFastActual, c='r', label="Experimental")
    zeroLine = np.zeros(len(guyFastActual))
    trialIters = np.linspace(0, len(guyFastSims), len(guyFastSims))
    ax.plot(trialIters, zeroLine, 'k')
    # ax.set_title("Fast IOI (500 ms)", fontsize='25')
    plt.xticks(np.arange(0, len(guyFastSims), 5))
    plt.setp(ax.get_yticklabels(), fontsize=15)
    plt.setp(ax.get_xticklabels(), fontsize=15)
    # plt.legend()

    ax = plt.subplot(234)
    plt.scatter(range(len(rickySlowSims)), rickySlowSims, marker="D", label="Simulated")
    plt.scatter(range(len(rickySlowActual)), rickySlowActual, c='r', label="Experimental")
    zeroLine = np.zeros(len(rickySlowActual))
    trialIters = np.linspace(0, len(rickySlowActual), len(rickySlowActual))
    ax.plot(trialIters, zeroLine, 'k')
    plt.xticks(np.arange(0, len(rickySlowSims), 5))
    plt.setp(ax.get_yticklabels(), fontsize=15)
    plt.setp(ax.get_xticklabels(), fontsize=15)
    # plt.legend()

    ax = plt.subplot(235)
    plt.scatter(range(len(rickyMedSims)), rickyMedSims, marker="D", label="Simulated")
    plt.scatter(range(len(rickyMedActual)), rickyMedActual, c='r', label="Experimental")
    zeroLine = np.zeros(len(rickyMedActual))
    trialIters = np.linspace(0, len(rickyMedActual), len(rickyMedActual))
    ax.plot(trialIters, zeroLine, 'k')
    plt.xticks(np.arange(0, len(rickyMedSims), 5))
    plt.setp(ax.get_yticklabels(), fontsize=15)
    plt.setp(ax.get_xticklabels(), fontsize=15)
    # plt.legend()

    ax = plt.subplot(236)
    plt.scatter(range(len(rickyFastSims)), rickyFastSims, marker="D", label="Simulated")
    plt.scatter(range(len(rickyFastActual)), rickyFastActual, c='r', label="Experimental")
    zeroLine = np.zeros(len(rickyFastActual))
    trialIters = np.linspace(0, len(rickyFastActual), len(rickyFastActual))
    ax.plot(trialIters, zeroLine, 'k')
    plt.xticks(np.arange(0, len(rickyFastSims), 5))
    plt.setp(ax.get_yticklabels(), fontsize=15)
    plt.setp(ax.get_xticklabels(), fontsize=15)
    # plt.legend()

    handles, labels = ax.get_legend_handles_labels()
    # fig.legend(handles, labels, loc='upper right', prop={'size': 17})
    plt.show()

graph_params(guyData, rickyData)