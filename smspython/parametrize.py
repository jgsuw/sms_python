import matplotlib.pyplot as plt
from scipy.optimize import minimize
import scipy.stats as stats
import numpy as np
import math
import pickle
import sys
import os
import simulate

experiment_data_pwd = "exp_data"
args = sys.argv
if len(args) < 2:
    print("No subject specified")
    sys.exit()
subject = args[1]
subject_dir = os.path.join(experiment_data_pwd, subject)

def get_file_path(subject_dir):

    trials = os.listdir(subject_dir)
    for i in range(len(trials)):
        print(i+1, ". ", trials[i])
    userChoice = int(input("Choose trial to Parametrize: "))
    userChoice -= 1
    filename = trials[userChoice]

    filepath = os.path.join(subject_dir, filename)
    return filepath

def grab_data(filepath):
    infile = open(filepath, 'rb')
    trialData = pickle.load(infile)
    infile.close()

    return trialData

def post_process(timeStamps_m, timeStamps_h):
    # go through each user tap and associate it with a given flash
    processData_level1 = []
    for i in range(len(timeStamps_h)):
        userTap = timeStamps_h[i]
        if len(timeStamps_m) > 0:
            assocFlash = timeStamps_m[0]
            assocDistance = math.fabs(assocFlash - userTap)
            assocIndex = 0  # Note: The assoc index is used to identify
            #       if multiple taps are associated with the
        for j in range(len(timeStamps_m)):  # same tap
            currFlash = timeStamps_m[j]
            currDistance = math.fabs(currFlash - userTap)
            if currDistance < assocDistance:
                assocDistance = currDistance
                assocFlash = currFlash
                assocIndex = j
        temp = [userTap, assocFlash, assocIndex]
        processData_level1.append(temp)

    # print("Process Data Level 1: ")
    # print(processData_level1)

    # now go through and see for the user taps that share associated flashes, which one is the closest
    processData_level2 = []
    maxIter = processData_level1[-1][2] + 1
    for i in range(maxIter):
        temp = []
        seekIndex = i
        for j in range(len(processData_level1)):
            if seekIndex == processData_level1[j][2]:
                temp.append(processData_level1[j])

        # print("Temp: ", temp)

        if len(temp) >= 1:
            bestFit = temp[0]
            closestDist = math.fabs(bestFit[1] - bestFit[0])
        # else:
        #     bestFit = temp[0]
        #     processData_level2.append(bestFit)
        for j in range(len(temp)):
            currFit = temp[j]
            currDist = math.fabs(currFit[1] - currFit[0])
            if currDist < closestDist:
                bestFit = currFit
                closestDist = currDist
        if len(temp) >= 1:
            processData_level2.append(bestFit)

    # print("Process Data Level 2")
    # print(processData_level2)

    # now see which tap times don't have an associated best tap
    processData_level3 = []
    for i in range(len(timeStamps_m)):
        currMacTime = timeStamps_m[i]
        hasAssocTap = False
        flashIndex = None
        for j in range(len(processData_level2)):
            assocTapTime = processData_level2[j][1]
            if currMacTime == assocTapTime:
                hasAssocTap = True
                flashIndex = j
        if hasAssocTap == False:
            userTap = None
            assocFlash = currMacTime
            temp = [userTap, assocFlash]
            processData_level3.append(temp)
        else:
            userTap = processData_level2[flashIndex][0]
            assocFlash = processData_level2[flashIndex][1]
            temp = [userTap, assocFlash]
            processData_level3.append(temp)

    return processData_level3

def calc_asyns(processedData):

    asynData = []
    for i in range(len(processedData)):
        userTap = processedData[i][0]
        machineTap = processedData[i][1]
        if userTap is not None:                   # later, look into what to do if userTap happens to be none
            asyn_h = userTap - machineTap
            asynData.append(asyn_h)
    return asynData

def calc_cost(simulatedAsyn, measuredAsyns):

    numIters = len(measuredAsyns)
    cost = 0
    for i in range(numIters):
        cost += (simulatedAsyn[i] - measuredAsyns[i])**2
    return cost

def objective_function(x, y):

    # package x, y as args to send into simulation
    alpha = x[0]
    beta = x[1]
    measuredAsyns = y
    i = int(len(measuredAsyns)/2)
    mean = np.mean(measuredAsyns[i:-1])
    simulationParams = [alpha,beta,mean]
    firstTap_u = timeStamps_h[0]

    args = (simulationParams, var, measuredAsyns, trialTm, firstTap_u)
    simulatedData = simulate.main(*args)
    simulatedAsyn = simulatedData[1]

    # cost = calc_cost(simulatedAsyn, measuredAsyns)
    cost = 0
    # cost += (np.average(simulatedAsyn)-np.average(measuredAsyns))**2
    # cost += (np.std(simulatedAsyn)-np.std(measuredAsyns))**2
    cost += np.sum((np.array(simulatedAsyn)-np.array(measuredAsyns))**2)
    # AC1 = np.corrcoef(measuredAsyns[1:-1],measuredAsyns[0:-2])[1,0]
    # AC1_s = np.corrcoef(simulatedAsyn[1:-1],simulatedAsyn[0:-2])
    # cost += (AC1-AC1_s)**2
    return cost

def sort_data(trialData):

    slowData = []
    midData = []
    fastData = []
    for i in range(len(trialData)):
        dataSet = trialData[i]
        trialName = dataSet["trialName"]

        if "slow" in trialName:
            slowData.append(dataSet)
        elif "mid" in trialName:
            midData.append(dataSet)
        elif "fast" in trialName:
            fastData.append(dataSet)

    sortedData = [slowData, midData, fastData]
    return sortedData

def calc_means_sds(slowMSD, midMSD, fastMSD):
    alphas = []
    betas = []
    for i in range(len(slowMSD)):
        alpha = slowMSD[i][4][0]
        beta = slowMSD[i][4][1]
        alphas.append(alpha)
        betas.append(beta)
    alphas = np.array(alphas)
    betas = np.array(betas)
    slowAlphaMean = np.mean(alphas)
    slowAlphaSD = np.std(alphas)
    slowBetaMean = np.mean(betas)
    slowBetaSD = np.std(betas)

    print("slow alpha mean: ",slowAlphaMean)
    print("slow alpha sd: ",slowAlphaSD)
    print("slow beta mean: ", slowBetaMean)
    print("slow beta sd: ", slowBetaSD)

    alphas = []
    betas = []
    for i in range(len(midMSD)):
        alpha = midMSD[i][4][0]
        beta = midMSD[i][4][1]
        alphas.append(alpha)
        betas.append(beta)
    alphas = np.array(alphas)
    betas = np.array(betas)
    midAlphaMean = np.mean(alphas)
    midAlphaSD = np.std(alphas)
    midBetaMean = np.mean(betas)
    midBetaSD = np.std(betas)

    print("mid alpha mean: ", midAlphaMean)
    print("mid alpha sd: ", midAlphaSD)
    print("mid beta mean: ", midBetaMean)
    print("mid beat sd: ", midBetaSD)

    alphas = []
    betas = []
    for i in range(len(fastMSD)):
        alpha = fastMSD[i][4][0]
        beta = fastMSD[i][4][1]
        alphas.append(alpha)
        betas.append(beta)
    alphas = np.array(alphas)
    betas = np.array(betas)
    fastAlphaMean = np.mean(alphas)
    fastAlphaSD = np.std(alphas)
    fastBetaMean = np.mean(betas)
    fastBetaSD = np.std(betas)

    print("fast alpha mean: ", fastAlphaMean)
    print("fast alpha sd: ", fastAlphaSD)
    print("fast beta mean: ", fastBetaMean)
    print("fast beta sd: ", fastBetaSD)

    statData = [slowAlphaMean, slowAlphaSD, slowBetaMean, slowBetaSD,
                midAlphaMean, midAlphaSD, midBetaMean, midBetaSD,
                fastAlphaMean, fastAlphaSD, fastBetaMean, fastBetaSD]
    return statData

def graph_distributions(statData):

    fig = plt.figure

    ax1 = plt.subplot(321)
    mu = statData[0]
    sigma = statData[1]
    x = np.linspace(mu - 3*sigma, mu + 3*sigma, 100)
    ax1.plot(x, stats.norm.pdf(x, mu, sigma))
    ax1.set_title("Slow Alpha Distribution")
    ax1.set_xlim([-5,5])

    ax2 = plt.subplot(322)
    mu = statData[2]
    sigma = statData[3]
    x = np.linspace(mu - 3*sigma, mu + 3*sigma, 100)
    ax2.plot(x, stats.norm.pdf(x, mu, sigma))
    ax2.set_title("Slow Beta Distribution")
    ax1.set_xlim([-5,5])

    ax3 = plt.subplot(323)
    mu = statData[4]
    sigma = statData[5]
    x = np.linspace(mu - 3*sigma, mu + 3*sigma, 100)
    ax3.plot(x, stats.norm.pdf(x, mu, sigma))
    ax3.set_title("Mid Alpha Distribution")

    ax4 = plt.subplot(324)
    mu = statData[6]
    sigma = statData[7]
    x = np.linspace(mu - 3*sigma, mu + 3*sigma, 100)
    ax4.plot(x, stats.norm.pdf(x, mu, sigma))
    ax4.set_title("Mid Beta Distribution")

    ax5 = plt.subplot(325)
    mu = statData[8]
    sigma = statData[9]
    x = np.linspace(mu - 3*sigma, mu + 3*sigma, 100)
    ax5.plot(x, stats.norm.pdf(x, mu, sigma))
    ax5.set_title("Fast Alpha Distribution")

    ax6 = plt.subplot(326)
    mu = statData[10]
    sigma = statData[11]
    x = np.linspace(mu - 3*sigma, mu + 3*sigma, 100)
    ax6.plot(x, stats.norm.pdf(x, mu, sigma))
    ax6.set_title("Fast Beta Distribution")

    for ax in [ax1,ax2,ax3,ax4,ax5,ax6]:
        ax.set_xlim([-1.5,.5])

    plt.tight_layout()
    plt.show()

def graph_fitted(slowMSD, midMSD, fastMSD):
    sortedData = [slowMSD, midMSD, fastMSD]
    for i in range(len(sortedData)):
        trialSpeed = sortedData[i]

        fig = plt.figure()
        ax1 = plt.subplot(321)
        # print(trialSpeed[0])
        measuredAsyns = trialSpeed[0][0]
        simAsyns = trialSpeed[0][1]
        trialName = trialSpeed[0][2]
        plt.scatter(range(len(measuredAsyns)), measuredAsyns, c='r', label="Experimental")
        plt.scatter(range(len(simAsyns)), simAsyns, label="Simulated")
        zeroLine = np.zeros(50)
        trialIters = np.linspace(0, 50, 50)
        ax1.plot(trialIters, zeroLine, 'k')
        ax1.set_title(trialName)
        ax1.set_xlabel("Tap Iteration")
        ax1.set_ylabel("Asynchrony (ms)")
        plt.legend()

        ax2 = plt.subplot(322)
        measuredAsyns = trialSpeed[1][0]
        simAsyns = trialSpeed[1][1]
        trialName = trialSpeed[1][2]
        plt.scatter(range(len(measuredAsyns)), measuredAsyns, c='r', label="Experimental")
        plt.scatter(range(len(simAsyns)), simAsyns, label="Simulated")
        zeroLine = np.zeros(50)
        trialIters = np.linspace(0, 50, 50)
        ax2.plot(trialIters, zeroLine, 'k')
        ax2.set_title(trialName)
        ax2.set_xlabel("Tap Iteration")
        ax2.set_ylabel("Asynchrony (ms)")
        plt.legend()

        ax3 = plt.subplot(323)
        measuredAsyns = trialSpeed[2][0]
        simAsyns = trialSpeed[2][1]
        trialName = trialSpeed[2][2]
        plt.scatter(range(len(measuredAsyns)), measuredAsyns, c='r', label="Experimental")
        plt.scatter(range(len(simAsyns)), simAsyns, label="Simulated")
        zeroLine = np.zeros(50)
        trialIters = np.linspace(0, 50, 50)
        ax3.plot(trialIters, zeroLine, 'k')
        ax3.set_title(trialName)
        ax3.set_xlabel("Tap Iteration")
        ax3.set_ylabel("Asynchrony (ms)")
        plt.legend()

        ax4 = plt.subplot(324)
        measuredAsyns = trialSpeed[3][0]
        simAsyns = trialSpeed[3][1]
        trialName = trialSpeed[3][2]
        plt.scatter(range(len(measuredAsyns)), measuredAsyns, c='r', label="Experimental")
        plt.scatter(range(len(simAsyns)), simAsyns, label="Simulated")
        zeroLine = np.zeros(50)
        trialIters = np.linspace(0, 50, 50)
        ax4.plot(trialIters, zeroLine, 'k')
        ax4.set_title(trialName)
        ax4.set_xlabel("Tap Iteration")
        ax4.set_ylabel("Asynchrony (ms)")
        plt.legend()

        ax5 = plt.subplot(325)
        measuredAsyns = trialSpeed[4][0]
        simAsyns = trialSpeed[4][1]
        trialName = trialSpeed[4][2]
        plt.scatter(range(len(measuredAsyns)), measuredAsyns, c='r', label="Experimental")
        plt.scatter(range(len(simAsyns)), simAsyns, label="Simulated")
        zeroLine = np.zeros(50)
        trialIters = np.linspace(0, 50, 50)
        ax5.plot(trialIters, zeroLine, 'k')
        ax5.set_title(trialName)
        ax5.set_xlabel("Tap Iteration")
        ax5.set_ylabel("Asynchrony (ms)")
        plt.legend()

        plt.show()


filepath = get_file_path(subject_dir)
trialData = grab_data(filepath)
sortedData = sort_data(trialData)
constraints = [
    # {'type' : 'ineq', 'fun' : lambda x: x[-1]-1},
    {'type' : 'ineq', 'fun' : lambda x: -x[0]},
    {'type' : 'ineq', 'fun' : lambda x: x[0]+1},
    {'type' : 'ineq', 'fun' : lambda x: -x[1]},
]

slowMSD = []
midMSD = []
fastMSD = []
for z in range(len(sortedData)):
    trialSpeed = sortedData[z]
    for k in range(len(trialSpeed)):
        trialName = trialSpeed[k]["trialName"]
        trialTm = trialSpeed[k]["trialTm"]
        timeStamps_h = trialSpeed[k]["timeStamps_h"]
        timeStamps_m = trialSpeed[k]["timeStamps_m"]

        processedData = post_process(timeStamps_m, timeStamps_h)
        y = calc_asyns(processedData)
        start = int(len(y)/2)
        humanMean = np.mean(y[start:-1])
        humanVar = np.var(y[start:-1])

        # print(trialName)
        # print(trialTm)
        # print(humanMean)
        # print(humanVar)

        f = lambda x: objective_function(x, y)

        AC1 = np.corrcoef(y[1:-1], y[0:-2])[1,0]
        AC2 = np.corrcoef(y[2:-1], y[0:-3])[1, 0]
        alpha = 1 - (AC2 / AC1)
        beta = -.2
        mean = humanMean
        var = humanVar

        guesses = []
        guess = (alpha, beta, mean)
        guesses.append(guess)

        results = []
        for i in range(len(guesses)):
            params = guesses[i]
            alpha, beta = params[0], params[1]
            mean = params[2]
            trys = 5
            tempResults = []
            for j in range(trys):
                x0 = [alpha, beta]
                var = 0
                result = minimize(f,x0,method='COBYLA', constraints=constraints)
                tempResults.append(result)
            sorted_results = sorted(tempResults, key=lambda z: z.fun)
            results.append(sorted_results[0])

        sorted_results = sorted(results, key= lambda z: z.fun)
        bestResults = sorted_results[0]
        alpha = bestResults.x[0]
        beta = bestResults.x[1]
        mean = humanMean
        simulationParams = [alpha, beta, mean]
        # var = humanVar

        var = 0
        args = (simulationParams, var, y, trialTm, timeStamps_h[0])
        simulatedData = np.array(simulate.main(*args))
        # print(simulationParams)
        simAsyns = simulatedData[1]
        temp = [y, simAsyns, trialName, trialTm, simulationParams]
        if z == 0:
            slowMSD.append(temp)
        elif z == 1:
            midMSD.append(temp)
        elif z == 2:
            fastMSD.append(temp)

statData = calc_means_sds(slowMSD, midMSD, fastMSD)
graph_distributions(statData)
graph_fitted(slowMSD, midMSD, fastMSD)











