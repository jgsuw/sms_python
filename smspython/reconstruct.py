import matplotlib.pyplot as plt
import numpy as np
import math
import pickle
import sys
import os

experiment_data_pwd = "exp_data"
args = sys.argv
if len(args) < 2:
    print("No subject specified")
    sys.exit()
subject = args[1]
subject_dir = os.path.join(experiment_data_pwd, subject)

def get_file_path(subject_dir):

    trials = os.listdir(subject_dir)
    for i in range(len(trials)):
        print(i+1, ". ", trials[i])
    userChoice = int(input("Choose trial to reconstruct: "))
    userChoice -= 1
    filename = trials[userChoice]

    filepath = os.path.join(subject_dir, filename)
    return filepath

def grab_data(filepath):
    infile = open(filepath, 'rb')
    trialData = pickle.load(infile)
    infile.close()

    return trialData

def sort_data(trialData):
    slowData = []
    midData = []
    fastData = []
    for i in range(len(trialData)):
        dataSet = trialData[i]
        trialName = dataSet["trialName"]

        if trialName == "slow1" or trialName == "slow2" or trialName == "slow3" or trialName == "slow4" or trialName == "slow5":
            slowData.append(dataSet)
        elif trialName == "mid1" or trialName == "mid2" or trialName == "mid3" or trialName == "mid4" or trialName == "mid5":
            midData.append(dataSet)
        elif trialName == "fast1" or trialName == "fast2" or trialName == "fast3" or trialName == "fast4" or trialName == "fast5":
            fastData.append(dataSet)

    sortedData = [slowData, midData, fastData]
    return sortedData

def post_process(timeStamps_m, timeStamps_h):
    # go through each user tap and associate it with a given flash
    processData_level1 = []
    for i in range(len(timeStamps_h)):
        userTap = timeStamps_h[i]
        if len(timeStamps_m) > 0:
            assocFlash = timeStamps_m[0]
            assocDistance = math.fabs(assocFlash - userTap)
            assocIndex = 0  # Note: The assoc index is used to identify
            #       if multiple taps are associated with the
        for j in range(len(timeStamps_m)):  # same tap
            currFlash = timeStamps_m[j]
            currDistance = math.fabs(currFlash - userTap)
            if currDistance < assocDistance:
                assocDistance = currDistance
                assocFlash = currFlash
                assocIndex = j
        temp = [userTap, assocFlash, assocIndex]
        processData_level1.append(temp)

    # print("Process Data Level 1: ")
    # print(processData_level1)

    # now go through and see for the user taps that share associated flashes, which one is the closest
    processData_level2 = []
    if len(processData_level2) > 0:
        maxIter = processData_level1[-1][2] + 1
    for i in range(maxIter):
        temp = []
        seekIndex = i
        for j in range(len(processData_level1)):
            if seekIndex == processData_level1[j][2]:
                temp.append(processData_level1[j])

        # print("Temp: ", temp)

        if len(temp) >= 1:
            bestFit = temp[0]
            closestDist = math.fabs(bestFit[1] - bestFit[0])
        # else:
        #     bestFit = temp[0]
        #     processData_level2.append(bestFit)
        for j in range(len(temp)):
            currFit = temp[j]
            currDist = math.fabs(currFit[1] - currFit[0])
            if currDist < closestDist:
                bestFit = currFit
                closestDist = currDist
        if len(temp) >= 1:
            processData_level2.append(bestFit)

    # print("Process Data Level 2")
    # print(processData_level2)

    # now see which tap times don't have an associated best tap
    processData_level3 = []
    for i in range(len(timeStamps_m)):
        currMacTime = timeStamps_m[i]
        hasAssocTap = False
        flashIndex = None
        for j in range(len(processData_level2)):
            assocTapTime = processData_level2[j][1]
            if currMacTime == assocTapTime:
                hasAssocTap = True
                flashIndex = j
        if hasAssocTap == False:
            userTap = None
            assocFlash = currMacTime
            temp = [userTap, assocFlash]
            processData_level3.append(temp)
        else:
            userTap = processData_level2[flashIndex][0]
            assocFlash = processData_level2[flashIndex][1]
            temp = [userTap, assocFlash]
            processData_level3.append(temp)

    return processData_level3

def calc_asyns(processedData):

    asynData = []
    for i in range(len(processedData)):
        userTap = processedData[i][0]
        machineTap = processedData[i][1]
        if userTap is not None:                   # later, look into what to do if userTap happens to be none
            asyn_h = userTap - machineTap
            asynData.append(asyn_h)
    return asynData

def graph_exp(sortedData):

    slowData = sortedData[0]
    midData = sortedData[1]
    fastData = sortedData[2]

    slowAsyns = []
    midAsyns = []
    fastAsyns = []

    temp = []
    for i in range(len(slowData)):
        slowTrial = slowData[i]
        trialName = slowTrial["trialName"]
        timeStamps_h = slowTrial["timeStamps_h"]
        timeStamps_m = slowTrial["timeStamps_m"]

        processedData = post_process(timeStamps_m, timeStamps_h)
        asynData = calc_asyns(processedData)
        temp.append([trialName, asynData])
    for i in range(len(temp)):
        slowAsyns.append(temp[i])
    # print(slowAsyns)

    temp = []
    for i in range(len(midData)):
        midTrial = midData[i]
        trialName = midTrial["trialName"]
        timeStamps_h = midTrial["timeStamps_h"]
        timeStamps_m = midTrial["timeStamps_m"]

        processedData = post_process(timeStamps_m, timeStamps_h)
        asynData = calc_asyns(processedData)
        temp.append([trialName, asynData])
    for i in range(len(temp)):
        midAsyns.append(temp[i])
    # print(midAsyns)


    temp = []
    for i in range(len(fastData)):
        fastTrial = fastData[i]
        trialName = fastTrial["trialName"]
        timeStamps_h = fastTrial["timeStamps_h"]
        timeStamps_m = fastTrial["timeStamps_m"]

        processedData = post_process(timeStamps_m, timeStamps_h)
        asynData = calc_asyns(processedData)
        temp.append([trialName, asynData])
    for i in range(len(temp)):
        fastAsyns.append(temp[i])
    # print(fastAsyns)

    fig = plt.figure()
    ax1 = plt.subplot(311)
    for i in range(len(slowAsyns)):
        dataLabel = slowAsyns[i][0]
        asynData = slowAsyns[i][1]
        graphIters = np.linspace(0, len(asynData), len(asynData))
        ax1.plot(graphIters, asynData, 'o-', label=dataLabel)
    ax1.set_title("Slow Trial Asyns")
    ax1.set_xlabel("Tap Iteration")
    ax1.set_ylabel("Asynchrony (ms)")
    plt.legend()

    ax2 = plt.subplot(312)
    for i in range(len(midAsyns)):
        dataLabel = midAsyns[i][0]
        asynData = midAsyns[i][1]
        graphIters = np.linspace(0, len(asynData), len(asynData))
        ax2.plot(graphIters, asynData, 'o-', label=dataLabel)
    ax2.set_title("Mid Trial Asyns")
    ax2.set_xlabel("Tap Iteration")
    ax2.set_ylabel("Asynchrony (ms)")
    plt.legend()

    ax3 = plt.subplot(313)
    for i in range(len(fastAsyns)):
        dataLabel = fastAsyns[i][0]
        asynData = fastAsyns[i][1]
        graphIters = np.linspace(0, len(asynData), len(asynData))
        ax3.plot(graphIters, asynData, 'o-', label=dataLabel)
    ax3.set_title("Fast Trial Asyns")
    ax3.set_xlabel("Tap Iteration")
    ax3.set_ylabel("Asynhrony (ms)")

    plt.legend()
    plt.show()

# def graph_trial_asyns(trialData):
#
#     # FIRST trial
#     currDataSet = trialData[0]
#     trialName = currDataSet["trialName"]
#     trialTm = currDataSet["trialTm"]
#     timeStamps_h = currDataSet["timeStamps_h"]
#     timeStamps_m = currDataSet["timeStamps_m"]
#
#     masterList = post_process(timeStamps_m, timeStamps_h)
#
#     asynData = []
#     for i in range(len(masterList)):
#         userTap = masterList[i][0]
#         machineTap = masterList[i][1]
#         if userTap is not None:
#             asyn_h = userTap - machineTap
#             asynData.append(asyn_h)
#
#     fig = plt.figure()
#     ax1 = plt.subplot(321)
#     trialIters = np.linspace(0, len(asynData), len(asynData))
#     ax1.plot(trialIters, asynData, 'ko', label="Asyn Over Iteration")
#     ax1.set_title(trialName)
#     ax1.set_xlabel("Time (TBD)")
#     ax1.set_ylabel("Asyn (ms)")
#     zeroLine = np.linspace(0, 1, len(asynData))
#     ax1.plot(trialIters, zeroLine, 'k--')
#
#     # SECOND trial
#     currDataSet = trialData[1]
#     trialName = currDataSet["trialName"]
#     trialTm = currDataSet["trialTm"]
#     timeStamps_h = currDataSet["timeStamps_h"]
#     timeStamps_m = currDataSet["timeStamps_m"]
#
#     masterList = post_process(timeStamps_m, timeStamps_h)
#
#     asynData = []
#     for i in range(len(masterList)):
#         userTap = masterList[i][0]
#         machineTap = masterList[i][1]
#         if userTap is not None:
#             asyn_h = userTap - machineTap
#             asynData.append(asyn_h)
#
#     ax2 = plt.subplot(322)
#     trialIters = np.linspace(0, len(asynData), len(asynData))
#     ax2.plot(trialIters, asynData, 'ko', label="Asyn Over Iteration")
#     ax2.set_title(trialName)
#     ax2.set_xlabel("Time (TBD)")
#     ax2.set_ylabel("Asyn (ms)")
#     zeroLine = np.linspace(0, 1, len(asynData))
#     ax2.plot(trialIters, zeroLine, 'k--')
#
#     # THIRD trial
#     currDataSet = trialData[2]
#     trialName = currDataSet["trialName"]
#     trialTm = currDataSet["trialTm"]
#     timeStamps_h = currDataSet["timeStamps_h"]
#     timeStamps_m = currDataSet["timeStamps_m"]
#
#     masterList = post_process(timeStamps_m, timeStamps_h)
#
#     asynData = []
#     for i in range(len(masterList)):
#         userTap = masterList[i][0]
#         machineTap = masterList[i][1]
#         if userTap is not None:
#             asyn_h = userTap - machineTap
#             asynData.append(asyn_h)
#
#     ax3 = plt.subplot(323)
#     trialIters = np.linspace(0, len(asynData), len(asynData))
#     ax3.plot(trialIters, asynData, 'ko', label="Asyn Over Iteration")
#     ax3.set_title(trialName)
#     ax3.set_xlabel("Time (TBD)")
#     ax3.set_ylabel("Asyn (ms)")
#     zeroLine = np.linspace(0, 1, len(asynData))
#     ax3.plot(trialIters, zeroLine, 'k--')
#
#     # FOURTH trial
#     currDataSet = trialData[3]
#     trialName = currDataSet["trialName"]
#     trialTm = currDataSet["trialTm"]
#     timeStamps_h = currDataSet["timeStamps_h"]
#     timeStamps_m = currDataSet["timeStamps_m"]
#
#     masterList = post_process(timeStamps_m, timeStamps_h)
#
#     asynData = []
#     for i in range(len(masterList)):
#         userTap = masterList[i][0]
#         machineTap = masterList[i][1]
#         if userTap is not None:
#             asyn_h = userTap - machineTap
#             asynData.append(asyn_h)
#
#     ax4 = plt.subplot(324)
#     trialIters = np.linspace(0, len(asynData), len(asynData))
#     ax4.plot(trialIters, asynData, 'ko', label="Asyn Over Iteration")
#     ax4.set_title(trialName)
#     ax4.set_xlabel("Time (TBD)")
#     ax4.set_ylabel("Asyn (ms)")
#     zeroLine = np.linspace(0, 1, len(asynData))
#     ax4.plot(trialIters, zeroLine, 'k--')
#
#     # FIFTH trial
#     currDataSet = trialData[4]
#     trialName = currDataSet["trialName"]
#     trialTm = currDataSet["trialTm"]
#     timeStamps_h = currDataSet["timeStamps_h"]
#     timeStamps_m = currDataSet["timeStamps_m"]
#
#     masterList = post_process(timeStamps_m, timeStamps_h)
#
#     asynData = []
#     for i in range(len(masterList)):
#         userTap = masterList[i][0]
#         machineTap = masterList[i][1]
#         if userTap is not None:
#             asyn_h = userTap - machineTap
#             asynData.append(asyn_h)
#
#     ax5 = plt.subplot(325)
#     trialIters = np.linspace(0, len(asynData), len(asynData))
#     ax5.plot(trialIters, asynData, 'ko', label="Asyn Over Iteration")
#     ax5.set_title(trialName)
#     ax5.set_xlabel("Time (TBD)")
#     ax5.set_ylabel("Asyn (ms)")
#     zeroLine = np.linspace(0, 1, len(asynData))
#     ax5.plot(trialIters, zeroLine, 'k--')
#
#     # SIXTH trial
#     currDataSet = trialData[5]
#     trialName = currDataSet["trialName"]
#     trialTm = currDataSet["trialTm"]
#     timeStamps_h = currDataSet["timeStamps_h"]
#     timeStamps_m = currDataSet["timeStamps_m"]
#
#     masterList = post_process(timeStamps_m, timeStamps_h)
#
#     asynData = []
#     for i in range(len(masterList)):
#         userTap = masterList[i][0]
#         machineTap = masterList[i][1]
#         if userTap is not None:
#             asyn_h = userTap - machineTap
#             asynData.append(asyn_h)
#
#     ax6 = plt.subplot(326)
#     trialIters = np.linspace(0, len(asynData), len(asynData))
#     ax6.plot(trialIters, asynData, 'ko', label="Asyn Over Iteration")
#     ax6.set_title(trialName)
#     ax6.set_xlabel("Time (TBD)")
#     ax6.set_ylabel("Asyn (ms)")
#     zeroLine = np.linspace(0, 1, len(asynData))
#     ax6.plot(trialIters, zeroLine, 'k--')
#
#     plt.tight_layout()
#     plt.show()

filepath = get_file_path(subject_dir)
trialData = grab_data(filepath)
sortedData = sort_data(trialData)
graph_exp(sortedData)

# graph_trial_asyns(trialData)