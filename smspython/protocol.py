from scipy.optimize import minimize
import matplotlib.pyplot as plt
import numpy as np
import pygame
import sys, os
import math, random

def main(trialName, trialTm, adaptiveParams, trialPeriod=45e3):

    """ len(adaptiveParams) = 3 """
    adaptive = adaptiveParams[0]
    alpha_m, beta_m = 0, 0
    if adaptive:
        _, alpha_m, beta_m = adaptiveParams

    # initialize pygame engine
    pygame.init()
    black = (0, 0, 0)
    red = (255, 0, 0)
    green = (0, 255, 0)
    displaySize = displayWidth, displayHeight = 1400, 800
    gameDisplay = pygame.display.set_mode(displaySize)

    # initialization of simulation parameters
    Tm = [trialTm]
    now = pygame.time.get_ticks()
    tm = [now + Tm[0]]

    lastFlash = None
    nextFlash = tm[-1]
    hasTapped = False

    timerPeriod = Tm[-1]
    print(timerPeriod)
    pygame.time.set_timer(pygame.USEREVENT, int(timerPeriod))

    drawGreen = False
    drawRed = False
    start = pygame.time.get_ticks()

    doContinue = True
    tapTime = None

    fps = 240
    clock = pygame.time.Clock()

    timeStamps_h = []
    timeStamps_m = []
    tempTapTimes = []

    gameDisplay.fill(black)
    pygame.display.flip()
    while doContinue and pygame.time.get_ticks()-start < trialPeriod:        # bump to thirty seconds after ironing out

        # start a timer for x second limit
        timeElapsed = pygame.time.get_ticks()

        for event in pygame.event.get():

            # if user quits
            if event.type == pygame.QUIT:
                sys.exit()

            if event.type == pygame.KEYDOWN:
                tapTime = pygame.time.get_ticks()
                timeStamps_h.append(tapTime)
                tempTapTimes.append(tapTime)

            if event.type == pygame.USEREVENT:
                # draws red
                if drawGreen:
                    drawGreen = False
                    drawRed = True
                    if adaptive:
                        lastFlash = timeStamps_m[-1]

                        # check to see if user has tapped, and if they have get the closest tap
                        if len(tempTapTimes) > 0:
                            closestTap = tempTapTimes[0]
                            for i in range(len(tempTapTimes)):
                                currTap = tempTapTimes[i]
                                if math.fabs(currTap - lastFlash) < math.fabs(closestTap - lastFlash):
                                    closestTap = currTap

                            # print("tap time", tapTime)
                            # print("last flash", lastFlash)
                            # print("current flash", currentFlash)
                            asyn = closestTap - lastFlash

                            dt = Tm[-1] + (alpha_m + beta_m) * asyn
                            next_Tm = Tm[-1] + beta_m * asyn
                            Tm.append(next_Tm)
                            pygame.time.set_timer(pygame.USEREVENT, int(dt / 2))
                            # print("asyn: ", asyn)
                            # print("dt: ", dt)

                        tempTapTimes = []

                # draws green
                else:
                    drawGreen = True
                    drawRed = False
                    # print("Green Flash")
                    displayTime = pygame.time.get_ticks()
                    timeStamps_m.append(displayTime)

                gameDisplay.fill(black)
                if drawGreen:
                    pygame.draw.circle(gameDisplay, green, (int(displayWidth/3), int(displayHeight/2)), 100)
                elif drawRed:
                    pygame.draw.circle(gameDisplay, red, (int(2 * displayWidth / 3), int(displayHeight/2)), 100)

                pygame.display.flip()
        # clock.tick(fps)

    data = {
        "trialName": trialName,
        "trialTm": trialTm,
        "adaptiveParams": adaptiveParams,
        "timeStamps_h": timeStamps_h,
        "timeStamps_m": timeStamps_m,
        "trialDuration": trialPeriod,
        "version": "2019-Aug-12"
    }

    # print("Foo")
    pygame.quit()

    if trialName != "testRun":
        return data