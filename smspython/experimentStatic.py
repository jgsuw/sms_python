import matplotlib.pyplot as plt
import pickle
import numpy as np
import pygame
import sys
import os
import time
import math
from random import shuffle
import protocol

args = sys.argv
testRun = True
numIters = 600
trialType = "-Static"
# TODO: Figure out how many of each trial you need to run for each static case
#       what are the most appropriate values, and where should we pickle the data to
# TODO: Anonymize subject name with alphanumeric string
def setup(args):
    args = sys.argv
    if len(args) < 2:
        print("No subject specified.")
        sys.exit()
    subject = args[1]
    exp_data_dir = "exp_data"
    subject_dir = os.path.join(exp_data_dir,subject)
    if not os.path.exists(subject_dir):
        os.mkdir(subject_dir)

    return subject, subject_dir

def datestring(t=None,sec=False):
  """
  Datestring

  Inputs:
    (optional)
    t - time.localtime()
    sec - bool - whether to include sec [SS] in output

  Outputs:
    ds - str - date in YYYYMMDD-HHMM[SS] format

  by Sam Burden 2012
  """
  if t is None:
    import time
    t = time.localtime()

  ye = '%04d'%t.tm_year
  mo = '%02d'%t.tm_mon
  da = '%02d'%t.tm_mday
  ho = '%02d'%t.tm_hour
  mi = '%02d'%t.tm_min
  se = '%02d'%t.tm_sec
  if not sec:
    se = ''

  return ye+mo+da+'-'+ho+mi+se

def pickle_data(subject, subject_dir, trialData):
    filename = datestring()
    filename += subject
    filename += trialType
    filename += ".pickle"

    pkl_file = os.path.join(subject_dir, filename)
    save_file = open(pkl_file, "wb")
    pickle.dump(trialData, save_file)
    save_file.close()

def get_experimental_data():

    args = []
    trialData = []

    # a test trial to have the user just get a trial where they can
    # attempt to synchronize -- no adaptation, no data collection
    if testRun:
        trialName = "testRun"
        trialTm = .6e3

        isAdaptive = False
        alpha_m = 0
        beta_m = 0
        adaptiveParams = [isAdaptive, alpha_m, beta_m]
        args = (trialName, trialTm, adaptiveParams)
        protocol.main(*args)

    time.sleep(5)
    experimentOrder = ["slow1", "slow2", "slow3", "slow4", "slow5",
                       "mid1", "mid2", "mid3", "mid4", "mid5",
                       "fast1", "fast2", "fast3", "fast4", "fast5"]

    shuffle(experimentOrder)

    print(experimentOrder)
    for i in range(len(experimentOrder)):
        trialName = experimentOrder[i]
        print(trialName)
        print(i)

        trialTm = 0
        isAdaptive = False
        alpha_m = 0
        beta_m = 0

        if "slow" in trialName:
            trialTm = .9e3
            isAdaptive = False
            alpha_m = 0
            beta_m = 0

        elif "mid" in trialName:
            trialTm = .65e3
            isAdaptive = False
            alpha_m = 0
            beta_m = 0

        elif "fast" in trialName:
            trialTm = .5e3
            isAdaptive = False
            alpha_m = 0
            beta_m = 0

        else:
            raise RuntimeError()

        adaptiveParams = [isAdaptive, alpha_m, beta_m]
        args = (trialName, trialTm, adaptiveParams)
        trialData += [protocol.main(*args)]

        time.sleep(5)

    return trialData

subject, subject_dir = setup(args)
trialData = get_experimental_data()
pickle_data(subject, subject_dir, trialData)




