import protocol
import sys
import os
import pickle
import numpy as np
import matplotlib.pyplot as plt
import time

# TODO: instruct subjects that they will undergo three thirty-second
#       trials, and that their task is to tap the spacebar at the same
#       frequency at which the yellow ball appears on the black screen
# TODO: Give the three trials to subject in random order
# TODO: Add an additional arg order so you know how much to subtract from each
#       trial for alignment purposes

## PWD Establishment stuff
args = sys.argv
if len(args) < 2:
    print("No subject specified")
    sys.exit()
subject = args[1]
bpm_fold_pwd = "/Users/jimmy/PycharmProjects/hapticGames/bpm_game/bpm_data"
subject_dir = os.path.join(bpm_fold_pwd,subject)
if not os.path.exists(subject_dir):
    os.mkdir(subject_dir)

def datestring(t=None,sec=False):
  """
  Datestring

  Inputs:
    (optional)
    t - time.localtime()
    sec - bool - whether to include sec [SS] in output

  Outputs:
    ds - str - date in YYYYMMDD-HHMM[SS] format

  by Sam Burden 2012
  """
  if t is None:
    import time
    t = time.localtime()

  ye = '%04d'%t.tm_year
  mo = '%02d'%t.tm_mon
  da = '%02d'%t.tm_mday
  ho = '%02d'%t.tm_hour
  mi = '%02d'%t.tm_min
  se = '%02d'%t.tm_sec
  if not sec:
    se = ''

  return ye+mo+da+'-'+ho+mi+se

def get_trial_data():
    args = []
    trialData = []

    for i in range(3):
        if i == 0:
            trialName = "Convergence"
            startBPM = 105
            yIntercept = -40
            slope = 1.8
            args = (trialName, startBPM, yIntercept, slope)
            trialData += [protocol.main(*args)]

        elif i == 1:
            trialName = "Oscillation"
            startBPM = 50
            yIntercept = 135
            slope = -1.15
            args = (trialName, startBPM, yIntercept, slope)
            trialData += [protocol.main(*args)]

        else:
            trialName = "Divergence"
            startBPM = 45
            yIntercept = 24
            slope = .4
            args = (trialName, startBPM, yIntercept, slope)
            trialData += [protocol.main(*args)]
        time.sleep(5)
    return trialData

def graph_trial_data(trialData):

    maxBPM = 120
    minBPM = 35

    trialName = trialData[0]['trialName']
    slope = trialData[0]['slope']
    yIntercept = trialData[0]['yIntercept']
    xStar = trialData[0]['xStar']
    userBPMs = trialData[0]['userBPMs']
    machineBPMs = trialData[0]['machineBPMs']
    timeStamps = trialData[0]['timeStamps']

    numIters = maxBPM
    x_bpm = np.linspace(0,maxBPM,maxBPM)
    hGraphVals = x_bpm
    mGraphVals = yIntercept+slope*x_bpm

    ax = plt.subplot(321)
    ax.plot(x_bpm, hGraphVals, label="Preference Curve")
    ax.plot(x_bpm, mGraphVals, label="Control Curve")             # made the edit here
    ax.plot(machineBPMs, userBPMs, 'k--', label="HBPM v. MBPM")
    ax.plot(machineBPMs[1:-2], userBPMs[1:-2], 'o', color='blue')
    ax.plot(machineBPMs[0], userBPMs[0], marker='o', markersize=5, color="green")
    ax.plot(machineBPMs[-1], userBPMs[-1], marker='^', markersize=5, color="red")

    ax.set_xticks(np.arange(0, maxBPM, 20))
    ax.set_xlim([0,maxBPM])
    ax.set_ylim([0,maxBPM])
    ax.set_title("Trial 1: %s" % trialName)
    ax.set_xlabel("Machine BPM (Beats per Minute)")
    ax.set_ylabel("Human BPM (Beats per Minute)")

    ax = plt.subplot(322)
    ax.plot(timeStamps-timeStamps[0], userBPMs, 'k--')
    ax.plot(timeStamps[1:-2]-timeStamps[0], userBPMs[1:-2], 'o', color='blue')
    ax.set_title("User BPM Over Time")
    ax.set_xlabel("Time (Seconds)")
    ax.set_ylabel("User BPM (Beats per Minute)")
    ax.plot(0, userBPMs[0], marker='o', markersize=5, color="green")
    ax.plot(timeStamps[-1]-timeStamps[0], userBPMs[-1], marker='^', markersize=5, color="red")
    ax.set_xlim([0,timeStamps[-1]-timeStamps[0]])

    trialName = trialData[1]['trialName']
    slope = trialData[1]['slope']
    yIntercept = trialData[1]['yIntercept']
    xStar = trialData[1]['xStar']
    userBPMs = trialData[1]['userBPMs']
    machineBPMs = trialData[1]['machineBPMs']
    timeStamps = trialData[1]['timeStamps']

    numIters = maxBPM
    x_bpm = np.linspace(0, maxBPM, maxBPM)
    hGraphVals = x_bpm
    mGraphVals = yIntercept + slope * x_bpm

    ax = plt.subplot(323)
    ax.plot(x_bpm, hGraphVals, label="Preference Curve")
    ax.plot(x_bpm, mGraphVals, label="Control Curve")
    ax.plot(machineBPMs, userBPMs, 'k--', label="HBPM v. MBPM")
    ax.plot(machineBPMs[1:-2], userBPMs[1:-2], 'o', color='blue')
    ax.plot(machineBPMs[0], userBPMs[0], marker='o', markersize=5, color="green")
    ax.plot(machineBPMs[-1], userBPMs[-1], marker='^', markersize=5, color="red")

    ax.set_xticks(np.arange(0, maxBPM, 20))
    ax.set_xlim([0,maxBPM])
    ax.set_ylim([0,maxBPM])
    ax.set_title("Trial 2: %s" % trialName)
    ax.set_xlabel("Machine BPM (Beats per Minute)")
    ax.set_ylabel("Human BPM (Beats per Minute)")

    ax = plt.subplot(324)
    ax.plot(timeStamps-timeStamps[0], userBPMs, 'k--')
    ax.plot(timeStamps[1:-2]-timeStamps[0], userBPMs[1:-2], 'o', color='blue')
    ax.set_title("User BPM Over Time")
    ax.set_xlabel("Time (Seconds)")
    ax.set_ylabel("User BPM (Beats per Minute)")
    ax.plot(0, userBPMs[0], marker='o', markersize=5, color="green")
    ax.plot(timeStamps[-1]-timeStamps[0], userBPMs[-1], marker='^', markersize=5, color="red")
    ax.set_xlim([0,timeStamps[-1]-timeStamps[0]])

    trialName = trialData[2]['trialName']
    slope = trialData[2]['slope']
    yIntercept = trialData[2]['yIntercept']
    xStar = trialData[2]['xStar']
    userBPMs = trialData[2]['userBPMs']
    machineBPMs = trialData[2]['machineBPMs']
    timeStamps = trialData[2]['timeStamps']

    numIters = maxBPM
    x_bpm = np.linspace(0, maxBPM, maxBPM)
    hGraphVals = x_bpm
    mGraphVals = yIntercept + slope * x_bpm

    ax = plt.subplot(325)
    ax.plot(x_bpm, hGraphVals, label="Preference Curve")
    ax.plot(x_bpm, mGraphVals, label="Control Curve")
    ax.plot(machineBPMs, userBPMs, 'k--', label="HBPM v. MBPM")
    ax.plot(machineBPMs[1:-2], userBPMs[1:-2], 'o', color='blue')
    ax.plot(machineBPMs[0], userBPMs[0], marker='o', markersize=5, color="green")
    ax.plot(machineBPMs[-1], userBPMs[-1], marker='^', markersize=5, color="red")

    ax.set_xticks(np.arange(0, maxBPM, 20))
    ax.set_xlim([0,maxBPM])
    ax.set_ylim([0,maxBPM])
    ax.set_title("Trial 3: %s" % trialName)
    ax.set_xlabel("Machine BPM (Beats per Minute)")
    ax.set_ylabel("Human BPM (Beats per Minute)")

    ax = plt.subplot(326)
    ax.plot(timeStamps-timeStamps[0], userBPMs, 'k--')
    ax.plot(timeStamps[1:-2]-timeStamps[0], userBPMs[1:-2], 'o', color='blue')
    ax.set_title("User BPM Over Time")
    ax.set_xlabel("Time (Seconds)")
    ax.set_ylabel("User BPM (Beats per Minute)")
    ax.plot(0, userBPMs[0], marker='o', markersize=5, color="green")
    ax.plot(timeStamps[-1]-timeStamps[0], userBPMs[-1], marker='^', markersize=5, color="red")
    ax.set_xlim([0,timeStamps[-1]-timeStamps[0]])

    plt.show()

filename = datestring()
filename += subject
filename += ".pickle"

trialData = get_trial_data()

pkl_file = os.path.join(subject_dir, filename)
save_file = open(pkl_file, "wb")
pickle.dump(trialData, save_file)
save_file.close()

graph_trial_data(trialData)