import sys
import os
import pickle
import numpy as np
import matplotlib.pyplot as plt

bpm_fold_pwd = "/Users/jimmy/PycharmProjects/hapticGames/bpm_game/bpm_data"
args = sys.argv
if len(args) < 2:
    print("No subject specified")
    sys.exit()
subject = args[1]
subject_dir = os.path.join(bpm_fold_pwd, subject)

def get_file_path(subject_dir):

    trials = os.listdir(subject_dir)
    for i in range(len(trials)):
        print(i+1, ". ", trials[i])
    userChoice = int(input("Choose trial to reconstruct: "))
    userChoice -= 1
    filename = trials[userChoice]

    filepath = os.path.join(subject_dir, filename)
    return filepath

def grab_data(filepath):
    infile = open(filepath, 'rb')
    trialData = pickle.load(infile)
    infile.close()

    return trialData

def graph_trial_data(trialData):

    maxBPM = 120
    minBPM = 35

    trialName = trialData[0]['trialName']
    slope = trialData[0]['slope']
    yIntercept = trialData[0]['yIntercept']
    xStar = trialData[0]['xStar']
    userBPMs = trialData[0]['userBPMs']
    machineBPMs = trialData[0]['machineBPMs']
    timeStamps = trialData[0]['timeStamps']

    numIters = maxBPM
    x_bpm = np.linspace(0,maxBPM,maxBPM)
    hGraphVals = x_bpm
    mGraphVals = yIntercept+slope*x_bpm

    ax = plt.subplot(321)
    ax.plot(x_bpm, hGraphVals, label="Preference Curve")
    ax.plot(mGraphVals, x_bpm, label="Control Curve")
    ax.plot(machineBPMs, userBPMs, 'k--', label="HBPM v. MBPM")
    ax.plot(machineBPMs[1:-2], userBPMs[1:-2], 'o', color='blue')
    ax.plot(machineBPMs[0], userBPMs[0], marker='o', markersize=5, color="green")
    ax.plot(machineBPMs[-1], userBPMs[-1], marker='^', markersize=5, color="red")

    ax.set_xticks(np.arange(0, maxBPM, 20))
    ax.set_xlim([0,maxBPM])
    ax.set_ylim([0,maxBPM])
    ax.set_title("Trial 1: %s" % trialName)
    ax.set_xlabel("Machine BPM (Beats per Minute)")
    ax.set_ylabel("Human BPM (Beats per Minute)")

    ax = plt.subplot(322)
    ax.plot(timeStamps-timeStamps[0], userBPMs, 'k--')
    ax.plot(timeStamps[1:-2]-timeStamps[0], userBPMs[1:-2], 'o', color='blue')
    ax.set_title("User BPM Over Time")
    ax.set_xlabel("Time (Seconds)")
    ax.set_ylabel("User BPM (Beats per Minute)")
    ax.plot(0, userBPMs[0], marker='o', markersize=5, color="green")
    ax.plot(timeStamps[-1]-timeStamps[0], userBPMs[-1], marker='^', markersize=5, color="red")
    ax.set_xlim([0,timeStamps[-1]-timeStamps[0]])

    trialName = trialData[1]['trialName']
    slope = trialData[1]['slope']
    yIntercept = trialData[1]['yIntercept']
    xStar = trialData[1]['xStar']
    userBPMs = trialData[1]['userBPMs']
    machineBPMs = trialData[1]['machineBPMs']
    timeStamps = trialData[1]['timeStamps']

    numIters = maxBPM
    x_bpm = np.linspace(0, maxBPM, maxBPM)
    hGraphVals = x_bpm
    mGraphVals = yIntercept + slope * x_bpm

    ax = plt.subplot(323)
    ax.plot(x_bpm, hGraphVals, label="Preference Curve")
    ax.plot(mGraphVals, x_bpm, label="Control Curve")
    ax.plot(machineBPMs, userBPMs, 'k--', label="HBPM v. MBPM")
    ax.plot(machineBPMs[1:-2], userBPMs[1:-2], 'o', color='blue')
    ax.plot(machineBPMs[0], userBPMs[0], marker='o', markersize=5, color="green")
    ax.plot(machineBPMs[-1], userBPMs[-1], marker='^', markersize=5, color="red")

    ax.set_xticks(np.arange(0, maxBPM, 20))
    ax.set_xlim([0,maxBPM])
    ax.set_ylim([0,maxBPM])
    ax.set_title("Trial 2: %s" % trialName)
    ax.set_xlabel("Machine BPM (Beats per Minute)")
    ax.set_ylabel("Human BPM (Beats per Minute)")

    ax = plt.subplot(324)
    ax.plot(timeStamps-timeStamps[0], userBPMs, 'k--')
    ax.plot(timeStamps[1:-2]-timeStamps[0], userBPMs[1:-2], 'o', color='blue')
    ax.set_title("User BPM Over Time")
    ax.set_xlabel("Time (Seconds)")
    ax.set_ylabel("User BPM (Beats per Minute)")
    ax.plot(0, userBPMs[0], marker='o', markersize=5, color="green")
    ax.plot(timeStamps[-1]-timeStamps[0], userBPMs[-1], marker='^', markersize=5, color="red")
    ax.set_xlim([0,timeStamps[-1]-timeStamps[0]])

    trialName = trialData[2]['trialName']
    slope = trialData[2]['slope']
    yIntercept = trialData[2]['yIntercept']
    xStar = trialData[2]['xStar']
    userBPMs = trialData[2]['userBPMs']
    machineBPMs = trialData[2]['machineBPMs']
    timeStamps = trialData[2]['timeStamps']

    numIters = maxBPM
    x_bpm = np.linspace(0, maxBPM, maxBPM)
    hGraphVals = x_bpm
    mGraphVals = yIntercept + slope * x_bpm

    ax = plt.subplot(325)
    ax.plot(x_bpm, hGraphVals, label="Preference Curve")
    ax.plot(mGraphVals, x_bpm, label="Control Curve")
    ax.plot(machineBPMs, userBPMs, 'k--', label="HBPM v. MBPM")
    ax.plot(machineBPMs[1:-2], userBPMs[1:-2], 'o', color='blue')
    ax.plot(machineBPMs[0], userBPMs[0], marker='o', markersize=5, color="green")
    ax.plot(machineBPMs[-1], userBPMs[-1], marker='^', markersize=5, color="red")

    ax.set_xticks(np.arange(0, maxBPM, 20))
    ax.set_xlim([0,maxBPM])
    ax.set_ylim([0,maxBPM])
    ax.set_title("Trial 3: %s" % trialName)
    ax.set_xlabel("Machine BPM (Beats per Minute)")
    ax.set_ylabel("Human BPM (Beats per Minute)")

    ax = plt.subplot(326)
    ax.plot(timeStamps-timeStamps[0], userBPMs, 'k--')
    ax.plot(timeStamps[1:-2]-timeStamps[0], userBPMs[1:-2], 'o', color='blue')
    ax.set_title("User BPM Over Time")
    ax.set_xlabel("Time (Seconds)")
    ax.set_ylabel("User BPM (Beats per Minute)")
    ax.plot(0, userBPMs[0], marker='o', markersize=5, color="green")
    ax.plot(timeStamps[-1]-timeStamps[0], userBPMs[-1], marker='^', markersize=5, color="red")
    ax.set_xlim([0,timeStamps[-1]-timeStamps[0]])

    # plt.tight_layout()
    plt.show()

def graph_for_poster(trialData):

    fig = plt.figure()

    maxBPM = 120
    minBPM = 35

    trialName = trialData[1]['trialName']
    slope = trialData[1]['slope']
    yIntercept = trialData[1]['yIntercept']
    xStar = trialData[1]['xStar']
    userBPMs = trialData[1]['userBPMs']
    machineBPMs = trialData[1]['machineBPMs']
    timeStamps = trialData[1]['timeStamps']

    numIters = maxBPM
    x_bpm = np.linspace(0, maxBPM, maxBPM)
    hGraphVals = x_bpm
    mGraphVals = yIntercept + slope * x_bpm

    ax = plt.subplot(221)
    ax.plot(x_bpm, hGraphVals, label="Preference Curve")
    ax.plot(mGraphVals, x_bpm, label="Control Curve")
    ax.plot(machineBPMs, userBPMs, 'k--', label="HBPM v. MBPM")
    ax.plot(machineBPMs[1:-2], userBPMs[1:-2], 'o', color='blue')
    ax.plot(machineBPMs[0], userBPMs[0], marker='o', markersize=5, color="green")
    ax.plot(machineBPMs[-1], userBPMs[-1], marker='^', markersize=5, color="red")
    plt.setp(ax.get_yticklabels(), fontsize=15)
    plt.setp(ax.get_xticklabels(), fontsize=15)

    ax.set_xticks(np.arange(0, maxBPM, 20))
    ax.set_xlim([0,maxBPM])
    ax.set_ylim([0,maxBPM])
    # ax.set_title("Trial 2: %s" % trialName)
    ax.set_xlabel("Machine BPM (Beats per Minute)", fontsize='15')
    ax.set_ylabel("Human BPM (Beats per Minute)", fontsize='15')
    plt.setp(ax.get_yticklabels(), fontsize=15)
    plt.setp(ax.get_xticklabels(), fontsize=15)

    ax = plt.subplot(222)
    ax.plot(timeStamps-timeStamps[0], userBPMs, 'k--')
    ax.plot(timeStamps[1:-2]-timeStamps[0], userBPMs[1:-2], 'o', color='blue')
    # ax.set_title("User BPM Over Time")
    ax.set_xlabel("Time (Seconds)", fontsize='15')
    ax.set_ylabel("User BPM (Beats per Minute)", fontsize='15')
    ax.plot(0, userBPMs[0], marker='o', markersize=5, color="green")
    ax.plot(timeStamps[-1]-timeStamps[0], userBPMs[-1], marker='^', markersize=5, color="red")
    ax.set_xlim([0,timeStamps[-1]-timeStamps[0]])
    plt.setp(ax.get_yticklabels(), fontsize=15)
    plt.setp(ax.get_xticklabels(), fontsize=15)

    trialName = trialData[2]['trialName']
    slope = trialData[2]['slope']
    yIntercept = trialData[2]['yIntercept']
    xStar = trialData[2]['xStar']
    userBPMs = trialData[2]['userBPMs']
    machineBPMs = trialData[2]['machineBPMs']
    timeStamps = trialData[2]['timeStamps']

    numIters = maxBPM
    x_bpm = np.linspace(0, maxBPM, maxBPM)
    hGraphVals = x_bpm
    mGraphVals = yIntercept + slope * x_bpm

    ax = plt.subplot(223)
    ax.plot(x_bpm, hGraphVals, label="Preference Curve")
    ax.plot(mGraphVals, x_bpm, label="Control Curve")
    ax.plot(machineBPMs, userBPMs, 'k--', label="HBPM v. MBPM")
    ax.plot(machineBPMs[1:-2], userBPMs[1:-2], 'o', color='blue')
    ax.plot(machineBPMs[0], userBPMs[0], marker='o', markersize=5, color="green")
    ax.plot(machineBPMs[-1], userBPMs[-1], marker='^', markersize=5, color="red")
    plt.setp(ax.get_yticklabels(), fontsize=15)
    plt.setp(ax.get_xticklabels(), fontsize=15)

    ax.set_xticks(np.arange(0, maxBPM, 20))
    ax.set_xlim([0,maxBPM])
    ax.set_ylim([0,maxBPM])
    # ax.set_title("Trial 3: %s" % trialName)
    ax.set_xlabel("Machine BPM (Beats per Minute)", fontsize='15')
    ax.set_ylabel("Human BPM (Beats per Minute)", fontsize='15')

    ax = plt.subplot(224)
    ax.plot(timeStamps-timeStamps[0], userBPMs, 'k--')
    ax.plot(timeStamps[1:-2]-timeStamps[0], userBPMs[1:-2], 'o', color='blue')
    # ax.set_title("User BPM Over Time")
    ax.set_xlabel("Time (Seconds)", fontsize='15')
    ax.set_ylabel("User BPM (Beats per Minute)", fontsize='15')
    ax.plot(0, userBPMs[0], marker='o', markersize=5, color="green")
    ax.plot(timeStamps[-1]-timeStamps[0], userBPMs[-1], marker='^', markersize=5, color="red")
    ax.set_xlim([0,timeStamps[-1]-timeStamps[0]])
    plt.setp(ax.get_yticklabels(), fontsize=15)
    plt.setp(ax.get_xticklabels(), fontsize=15)

    # plt.tight_layout()
    plt.show()

filepath = get_file_path(subject_dir)
trialData = grab_data(filepath)
graph_trial_data(trialData)
graph_for_poster(trialData)