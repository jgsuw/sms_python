import sys, pygame
import pygame.mixer
import matplotlib.pyplot as plt
import numpy as np
import pylab
import random
import math

# TODO: Tweak generation variation
# TODO: Clean Up Data Gathering
# TODO: Extrapolate necessary data from adamDev trial:
#       slope, init bpm?, divStat, convIntervals, userBPMs,
#       machineBPMS, timestamps, divTime
def main(*args):

    print(args)
    trialName = args[0]
    startBPM = args[1]
    yIntercept = args[2]
    slope = args[3]

    pygame.init()

    # human preference curve
    def preference_curve(x):
        y = x
        return x

    # hardcoded these in -- if user goes beyond these they're more or less
    # running or crawling
    maxBPM = 120
    minBPM = 30

    # create functions to govern machine interaction ,
    # equilibrium, and update rules
    def control_func(x, yIntercept):
        y = (slope * x) + yIntercept
        return y

    # following functions are implemented for learning rate simulation
    def control_func_getX(userBPM):
        x = (userBPM - yIntercept) / slope
        return x

    def machine_cost(bpm, userBPM):
        targetBPM = control_func(userBPM, yIntercept)
        cost = (targetBPM - bpm)**2
        return cost

    def machine_update(bpm, userBPM):
        #lr = .75
        #lr = .25
        #lr = .5
        lr = .8
        cost = machine_cost(bpm, userBPM)
        if cost == 0:
            return bpm
        else:
            updateMBPM = bpm + (lr * (control_func_getX(userBPM) - bpm))
            return updateMBPM

    def get_xstar(slope, yIntercept):
        x = yIntercept / ( 1 - slope )
        return x

    xStar = get_xstar(slope, yIntercept)
    print("X-Star Value(s): ", xStar )

    # text and display initiation
    pygame.font.init()
    font = pygame.font.SysFont('default', 32)
    black = (0, 0, 0)
    grey = (220, 220, 220)
    yellow = (248, 248, 140)
    white = (255, 255, 255)

    displaySize = displayWidth, displayHeight = 1400, 800
    gameDisplay = pygame.display.set_mode(displaySize)

    # initialization of simulation parameters , bpm
    pressPeriod = None
    bpm = startBPM
    timerPeriod = 60 / bpm / 2 * 1000
    pygame.time.set_timer(pygame.USEREVENT, int(timerPeriod))
    drawShape = False

    # list initialization for data collection
    deltas = np.array([])
    userBPMs = np.array([])
    machineBPMs = np.array([])
    timeStamps = np.array([])

    lastPress = None
    textIMG = font.render('-', True, grey)

    timeElapsed = 0
    start = pygame.time.get_ticks()
    doContinue = True

    hasDiverged = False
    divergeTime = None
    hasConverged = False

    while doContinue and timeElapsed <= 30000:

        # start a timer for x second limit
        timeElapsed = pygame.time.get_ticks()-start

        for event in pygame.event.get():

            # if user quits
            if event.type == pygame.QUIT:
                sys.exit()

            # This block keeps track of time between key presses
            if event.type == pygame.KEYDOWN:
                currentTime = pygame.time.get_ticks()
                if lastPress is None:
                    pass
                # if clicks are less than 2 seconds apart, add to deltas
                elif (currentTime - lastPress) < 10000:
                    dt = (currentTime - lastPress) / 1000
                    deltas = np.append(deltas, [dt])
                # if greater, clear the array and start over
                else:
                    deltas = np.array([])
                lastPress = currentTime

                # after getting 3 deltas, clear out and add median to masterlist
                # of user BPMs
                if len(deltas) >= 3:
                    pressPeriod = np.median(deltas)

                    # add current BPMs to a list for graphing purposes, data collection
                    h_bpm = (60 / pressPeriod)
                    userBPMs = np.append(userBPMs, [h_bpm])
                    machineBPMs = np.append(machineBPMs, [bpm])
                    timeStamps = np.append(timeStamps, [(currentTime / 1000)])

                    # convergence / divergence status monitor
                    if (h_bpm > maxBPM) or (h_bpm < minBPM):     # potentially use pythagorean distance in case equil is extreme
                        hasDiverged = True
                        divergeTime = currentTime / 1000
                        doContinue = False

                    # clear list
                    deltas = np.delete(deltas, 0)

            # set frequency
            if event.type == pygame.USEREVENT:
                if drawShape:
                    drawShape = False
                else:
                    drawShape = True
                    if pressPeriod is not None:
                        h_bpm = (60 / pressPeriod)
                        bpm = machine_update(bpm, h_bpm)
                        bpm = min(maxBPM, max(minBPM,bpm))
                        currentTime = pygame.time.get_ticks()
                        timerPeriod = int(1000 * 60 / bpm / 2)
                        pygame.time.set_timer(pygame.USEREVENT, timerPeriod)

        if pressPeriod is not None:
            #textIMG = font.render('%d' % (60 / pressPeriod), True, grey)
            pass

        gameDisplay.fill(black)
        if drawShape:
            pygame.draw.circle(gameDisplay, yellow, (int(displayWidth/2), int(displayHeight/2)), 100)

        gameDisplay.blit(textIMG, (0, 0))
        pygame.display.flip()

    data = {
        'trialName': trialName,
        'startBPM': startBPM,
        'yIntercept': yIntercept,
        'slope': slope,
        'xStar': xStar,
        'userBPMs': userBPMs,
        'machineBPMs': machineBPMs,
        'timeStamps': timeStamps,
        'hasDiverged': hasDiverged,
        'divergeTime': divergeTime
    }

    return data