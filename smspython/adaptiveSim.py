import matplotlib.pyplot as plt
import numpy as np
import math
import pickle
import sys, os

experiment_data_pwd = "exp_data"
args = sys.argv
if len(args) < 2:
    print("No subject specified")
    sys.exit()
subject = args[1]
subject_dir = os.path.join(experiment_data_pwd, subject)

def get_file_path(subject_dir):

    trials = os.listdir(subject_dir)
    for i in range(len(trials)):
        print(i+1, ". ", trials[i])
    userChoice = int(input("Choose trial to Parametrize: "))
    userChoice -= 1
    filename = trials[userChoice]

    filepath = os.path.join(subject_dir, filename)
    return filepath

def grab_data(filepath):
    infile = open(filepath, 'rb')
    trialData = pickle.load(infile)
    infile.close()

    return trialData

def post_process(timeStamps_m, timeStamps_h):
    # go through each user tap and associate it with a given flash
    processData_level1 = []
    for i in range(len(timeStamps_h)):
        userTap = timeStamps_h[i]
        if len(timeStamps_m) > 0:
            assocFlash = timeStamps_m[0]
            assocDistance = math.fabs(assocFlash - userTap)
            assocIndex = 0  # Note: The assoc index is used to identify
            #       if multiple taps are associated with the
        for j in range(len(timeStamps_m)):  # same tap
            currFlash = timeStamps_m[j]
            currDistance = math.fabs(currFlash - userTap)
            if currDistance < assocDistance:
                assocDistance = currDistance
                assocFlash = currFlash
                assocIndex = j
        temp = [userTap, assocFlash, assocIndex]
        processData_level1.append(temp)

    # print("Process Data Level 1: ")
    # print(processData_level1)

    # now go through and see for the user taps that share associated flashes, which one is the closest
    processData_level2 = []
    maxIter = processData_level1[-1][2] + 1
    for i in range(maxIter):
        temp = []
        seekIndex = i
        for j in range(len(processData_level1)):
            if seekIndex == processData_level1[j][2]:
                temp.append(processData_level1[j])

        # print("Temp: ", temp)

        if len(temp) >= 1:
            bestFit = temp[0]
            closestDist = math.fabs(bestFit[1] - bestFit[0])
        # else:
        #     bestFit = temp[0]
        #     processData_level2.append(bestFit)
        for j in range(len(temp)):
            currFit = temp[j]
            currDist = math.fabs(currFit[1] - currFit[0])
            if currDist < closestDist:
                bestFit = currFit
                closestDist = currDist
        if len(temp) >= 1:
            processData_level2.append(bestFit)

    # print("Process Data Level 2")
    # print(processData_level2)

    # now see which tap times don't have an associated best tap
    processData_level3 = []
    for i in range(len(timeStamps_m)):
        currMacTime = timeStamps_m[i]
        hasAssocTap = False
        flashIndex = None
        for j in range(len(processData_level2)):
            assocTapTime = processData_level2[j][1]
            if currMacTime == assocTapTime:
                hasAssocTap = True
                flashIndex = j
        if hasAssocTap == False:
            userTap = None
            assocFlash = currMacTime
            temp = [userTap, assocFlash]
            processData_level3.append(temp)
        else:
            userTap = processData_level2[flashIndex][0]
            assocFlash = processData_level2[flashIndex][1]
            temp = [userTap, assocFlash]
            processData_level3.append(temp)

    return processData_level3

def sort_data(trialData):

    slowData = []
    midData = []
    fastData = []
    for i in range(len(trialData)):
        dataSet = trialData[i]
        trialName = dataSet["trialName"]

        if "slow" in trialName:
            slowData.append(dataSet)
        elif "mid" in trialName:
            midData.append(dataSet)
        elif "fast" in trialName:
            fastData.append(dataSet)

    sortedData = [slowData, midData, fastData]
    return sortedData

def calc_asyns(processedData):

    asynData = []
    for i in range(len(processedData)):
        userTap = processedData[i][0]
        machineTap = processedData[i][1]
        if userTap is not None:                   # later, look into what to do if userTap happens to be none
            asyn_h = userTap - machineTap
            asynData.append(asyn_h)
    return asynData

def graph_sim(masterList):
    fig = plt.figure()

    ax1 = plt.subplot(321)
    trialName = masterList[0]["trialName"]
    trialTm = masterList[0]["trialTm"]
    abVals_m = masterList[0]["abVals_m"]
    asyns = masterList[0]["asyns"]
    ax1.plot(range(len(asyns)), asyns, 'ro', label="Simulated Asyn")
    ax1.set_title(trialName)
    ax1.set_xlabel("Tap Iteration")
    ax1.set_ylabel("Asynchrony (ms)")
    plt.legend()

    ax1 = plt.subplot(322)
    trialName = masterList[1]["trialName"]
    trialTm = masterList[1]["trialTm"]
    abVals_m = masterList[1]["abVals_m"]
    asyns = masterList[1]["asyns"]
    ax1.plot(range(len(asyns)), asyns, 'ro', label="Simulatd Asyn")
    ax1.set_title(trialName)
    ax1.set_xlabel("Tap Iteration")
    ax1.set_ylabel("Asynchrony (ms)")
    plt.legend()

    ax1 = plt.subplot(323)
    trialName = masterList[2]["trialName"]
    trialTm = masterList[2]["trialTm"]
    abVals_m = masterList[2]["abVals_m"]
    asyns = masterList[2]["asyns"]
    ax1.plot(range(len(asyns)), asyns, 'ro', label="Simulated Asyn")
    ax1.set_title(trialName)
    ax1.set_xlabel("Tap Iteration")
    ax1.set_ylabel("Asynchrony (ms)")
    plt.legend()

    ax1 = plt.subplot(324)
    trialName = masterList[3]["trialName"]
    trialTm = masterList[3]["trialTm"]
    abVals_m = masterList[3]["abVals_m"]
    asyns = masterList[3]["asyns"]
    ax1.plot(range(len(asyns)), asyns, 'ro', label="Simulated Asyn")
    ax1.set_title(trialName)
    ax1.set_xlabel("Tap Iteration")
    ax1.set_ylabel("Asynchrony (ms)")
    plt.legend()

    ax1 = plt.subplot(325)
    trialName = masterList[4]["trialName"]
    trialTm = masterList[4]["trialTm"]
    abVals_m = masterList[4]["abVals_m"]
    asyns = masterList[4]["asyns"]
    ax1.plot(range(len(asyns)), asyns, 'ro', label="Simulated Asyn")
    ax1.set_title(trialName)
    ax1.set_xlabel("Tap Iteration")
    ax1.set_ylabel("Asynchrony (ms)")

    plt.legend()
    plt.show()

filepath = get_file_path(subject_dir)
trialData = grab_data(filepath)
sortedData = sort_data(trialData)

# choose trial speeds to simulate
trialSpeedToSim = sortedData[2]

# Below are loaded in manually
# alpha_h, beta_h = -0.40681079759631966, -0.12226617924860764
# alpha_h = -0.5
alpha_h = -0.9
beta_h = 0
mean_h, var_h = -.1, .01                                          # TODO: Get these from experimental data
alphas = np.linspace(.2, 1, 5)
masterList = []

# print(trialSpeedToSim[0])
for k in range(len(trialSpeedToSim)):
    thisTrial = trialSpeedToSim[k]
    trialName = thisTrial["trialName"]
    trialTm = thisTrial["trialTm"]
    alpha_m, beta_m = thisTrial["adaptiveParams"][1], 0.0
    print((alpha_m,beta_m))
    print(mean_h)
    timeStamps_h = thisTrial["timeStamps_h"]
    timeStamps_m = thisTrial["timeStamps_m"]
    processedData = post_process(timeStamps_m, timeStamps_h)
    measuredAsyns = calc_asyns(processedData)

    n = len(measuredAsyns)

    asyn = np.zeros(n)
    tm, Tm = np.zeros(n), np.zeros(n)
    th, Th = np.zeros(n), np.zeros(n)

    # initial conditions
    tm[0], Tm[0] = processedData[0][1], trialTm
    # th[0], Th[0] = processedData[0][0], processedData[0][0] + (beta_h) * measuredAsyns[0]
    th[0], Th[0] = processedData[0][0], trialTm
    asyn[0] = measuredAsyns[0]

    for i in range(1, len(asyn)):
        # gamma_h = np.random.normal(0, var_h)
        gamma_h = 0
        th[i] = th[i-1]+Th[i-1]+(alpha_h+beta_h)*(asyn[i-1]-mean_h) + gamma_h
        tm[i] = tm[i - 1] + Tm[i - 1] + (alpha_m + beta_m) * (asyn[i-1])
        Th[i] = Th[i - 1] + (beta_h)*(asyn[i-1]-mean_h)
        Tm[i] = Tm[i - 1] + (beta_m)*(asyn[i-1])
        asyn[i] = th[i]-tm[i]

    temp = {
        "trialName":trialName,
        "trialTm":trialTm,
        "abVals_m": [alpha_m, beta_m],
        "asyns": asyn
    }
    masterList.append(temp)

graph_sim(masterList)