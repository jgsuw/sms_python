import matplotlib.pyplot as plt
import numpy as np
import pickle, os
import sys
import math
import pylab

def main(*args):
    simulationParams = args[0]
    var_s = args[1]
    measuredAsyns = args[2]
    trialTm = args[3]
    firstTap_u = args[4]

    def adam_next_state(tn, Tn, asynn, alpha, beta, gamma):
        next_tn = tn + Tn + ((alpha + beta) * asynn) + gamma
        next_Tn = Tn + (beta * asynn)
        return next_tn, next_Tn

    def run_static_sim(simulationParams, var_s, measuredAsyns, trialTm):

        alpha_s, beta_s = simulationParams[0], simulationParams[1]
        mean_s = simulationParams[2]

        if (var_s < 0):
            print(var_s)
            # raise (ValueError())
            var_s = math.fabs(var_s)
        Tn_metro = trialTm
        tn_metro = 0
        Tn_adam = Tn_metro
        tn_adam = measuredAsyns[0]

        simIters = len(measuredAsyns)

        tmData = []
        asyn_sData = []
        for i in range(simIters):
            temp = [tn_adam, tn_metro]
            tmData.append(temp)

            gamma_s = 0
            if var_s != 0:
                gamma_s = np.random.normal(0, np.sqrt(var_s))
            asyn = tn_adam - tn_metro
            asyn_obsv = asyn - mean_s
            asyn_sData.append(asyn)
            tn_adam, Tn_adam = adam_next_state(tn_adam, Tn_adam, asyn_obsv, alpha_s, beta_s, gamma_s)

            tn_metro += Tn_metro
        data = [tmData, asyn_sData]
        return data

    data = run_static_sim(simulationParams, var_s, measuredAsyns, trialTm)
    return data

# if __name__ == "__main__":
#     alpha = -.5
#     beta = -.1
#     mu = 100
#     sigma = 25
#     x = [alpha,beta,mu,sigma]
#     Tm = 1000
#     data = main(x,60*[0],Tm,0)
#     print(np.average(data[1][50:-1]))
#     plt.stem(data[1])
#     plt.show()
